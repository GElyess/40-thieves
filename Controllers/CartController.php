<?php

namespace Controllers;

use PDO;

class CartController extends Controller
{
    private function removeCartItem($productId)
    {
        if (isset($productId, $_SESSION['cart'], $_SESSION['cart'][$productId]) && is_numeric($productId)) {
            unset($_SESSION['cart'][$productId]);
        }
    }

    private function updateCartItems()
    {
        foreach ($_POST as $k => $v) {
            if (strpos($k, 'quantity') !== false && is_numeric($v)) {
                $id = str_replace('quantity-', '', $k);
                $quantity = (int)$v;
                if (is_numeric($id) && isset($_SESSION['cart'][$id]) && $quantity > 0) {
                    $_SESSION['cart'][$id] = $quantity;
                }
            }
        }
        header('location: index.php?view=cart');
        exit;
    }

    private function purchaseCartItems()
    {
        foreach ($_POST as $k => $v) {
            if (strpos($k, 'quantity') !== false && is_numeric($v)) {
                $id = str_replace('quantity-', '', $k);
                $quantity = (int)$v;
                $time_now = (new \Datetime('now'))->format('Y-m-d H:i:s');

                $stmt = $this->pdo->prepare('SELECT * FROM products WHERE id = ?');
                $stmt->execute([$id]);
                $product = $stmt->fetch(PDO::FETCH_ASSOC);

                $new_quantity = ((int)$product["quantity"] - $quantity);
                if ($new_quantity <= 0) {
                    $new_quantity = 0;
                }
                $this->pdo->prepare('UPDATE products SET quantity=?, date_added=? WHERE id=?')->execute([$new_quantity, $time_now, $id]);
            }
        }
        header('Location: index.php?view=purchase');
        exit;
    }

    private function renderCartItems()
    {
        if (isset($_POST['product_id'], $_POST['quantity']) && is_numeric($_POST['product_id']) && is_numeric($_POST['quantity'])) {
            $product_id = (int)$_POST['product_id'];
            $quantity = (int)$_POST['quantity'];
            $stmt = $this->pdo->prepare('SELECT * FROM products WHERE id = ?');
            $stmt->execute([$_POST['product_id']]);
            $product = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($product && $quantity > 0) {
                if (isset($_SESSION['cart']) && is_array($_SESSION['cart'])) {
                    if (array_key_exists($product_id, $_SESSION['cart'])) {
                        $_SESSION['cart'][$product_id] += $quantity;
                    } else {
                        $_SESSION['cart'][$product_id] = $quantity;
                    }
                } else {
                    $_SESSION['cart'] = array($product_id => $quantity);
                }
            }
            header('location: index.php?view=cart');
            exit;
        }
    }

    public function index()
    {
        if (isset($_POST['update']) && isset($_SESSION['cart'])) {
            $this->updateCartItems();
        } else if (isset($_POST['purchase']) && isset($_SESSION['cart']) && !empty($_SESSION['cart'])) {
            $this->purchaseCartItems();
        } else if (isset($_GET['action']) && $_GET['action'] == 'remove') {
            $this->removeCartItem($_GET['productId']);
        } else {
            $this->renderCartItems();
        }


        $products_in_cart = isset($_SESSION['cart']) ? $_SESSION['cart'] : array();
        $products = array();
        $subtotal = 0.00;
        if ($products_in_cart) {
            $array_to_question_marks = implode(',', array_fill(0, count($products_in_cart), '?'));
            $stmt = $this->pdo->prepare('SELECT * FROM products WHERE id IN (' . $array_to_question_marks . ')');
            $stmt->execute(array_keys($products_in_cart));
            $products = $stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($products as $product) {
                $subtotal += (float)$product['price'] * (int)$products_in_cart[$product['id']];
            }
        }

        $this->render('cart.twig', [
            'products'                  => $products,
            'products_in_cart'          => $products_in_cart,
            'subtotal'                  => $subtotal
        ]);
    }

    public function render($path, $vars)
    {
        echo $this->twig->render(
            $path,
            [
                "check_products"            => empty($vars['products']),
                "products"                  => $vars['products'],
                "products_in_cart"          => $vars['products_in_cart'],
                "subtotal"                  => $vars['subtotal']
            ]
        );
    }
}
