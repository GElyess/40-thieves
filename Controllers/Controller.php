<?php

namespace Controllers;

abstract class Controller {
    protected $pdo;
    protected $twig;

    function __construct($pdo, $twig)
    {
        $this->pdo = $pdo;
        $this->twig = $twig;
    }

    abstract function index();
    abstract function render($path, $vars);
}
