<?php

namespace Controllers;

use PDO;

class HomeController extends Controller {
    function index(){
        $stmt = $this->pdo->prepare('SELECT * FROM products ORDER BY date_added DESC LIMIT 4');
        $stmt->execute();
        $recently_added_products = $stmt->fetchAll(PDO::FETCH_ASSOC);

        echo $this->render('home.twig', ["recently_added_products" => $recently_added_products]); 
    }

    function render($path, $vars){
        echo $this->twig->render($path, $vars); 
    }
}
