<?php

namespace Controllers;

use PDO;

class ProductController extends Controller
{

    function index()
    {
        if (isset($_GET['id'])) {
            $stmt = $this->pdo->prepare('SELECT * FROM products WHERE id = ?');
            $stmt->execute([$_GET['id']]);
            $product = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!$product) {
                exit('<p>Product does not exist!</p>');
            }
        } else {
            exit('<p>Product does not exist!</p>');
        }

        $this->render('product.twig', $product);
    }

    function render($path, $product)
    {
        echo $this->twig->render($path, [
                "product" => $product
            ]
        );
    }
}
