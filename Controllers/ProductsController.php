<?php

namespace Controllers;

use PDO;

class ProductsController extends Controller
{

    function index()
    {

        $num_products_on_each_view = 8;
        $current_view = isset($_GET['p']) && is_numeric($_GET['p']) ? (int)$_GET['p'] : 1;
        $stmt = $this->pdo->prepare('SELECT * FROM products ORDER BY date_added DESC LIMIT ?,?');
        $stmt->bindValue(1, ($current_view - 1) * $num_products_on_each_view, PDO::PARAM_INT);
        $stmt->bindValue(2, $num_products_on_each_view, PDO::PARAM_INT);
        $stmt->execute();
        $products = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $total_products = $this->pdo->query('SELECT * FROM products')->rowCount();
        $counted_products = count($products);

        $this->render('products.twig', [
            'total_products'            => $total_products,
            'products'                  => $products,
            'current_view'              => $current_view,
            'num_products_on_each_view' => $num_products_on_each_view,
            'counted_products'          => $counted_products
        ]);
    }

    function render($path, $vars)
    {
        echo $this->twig->render($path, $vars);
    }
}
