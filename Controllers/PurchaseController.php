<?php 
namespace Controllers;

class PurchaseController extends Controller {
    
    function index() {
        unset($_SESSION['cart']);
        $this->render('purchase.twig',[]);
    }

    function render($path, $vars){
        echo $this->twig->render($path,$vars);
    }
}
