<?php
session_start();

require_once __DIR__ . '/vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader('./views');
$twig = new \Twig\Environment($loader);

require_once './libraries/database.php';

$pdo = pdo_connect_mysql();

$path_views         = './views/';
$path_controllers   = './controllers/';

$view = isset($_GET['view']) && !empty($_GET['view']) && file_exists($path_controllers . $_GET['view'] . 'Controller.php')
    ? $_GET['view']
    : 'home';
$num_items = isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0;

echo $twig->render(
    'header.twig',
    [
        "num_items"         => $num_items,
        "local_title"       => ucfirst($view)
    ]
);

$routes = [
    "product"       => new Controllers\ProductController($pdo, $twig),
    "purchase"      => new Controllers\PurchaseController($pdo, $twig),
    "products"      => new Controllers\ProductsController($pdo, $twig),
    "home"          => new Controllers\HomeController($pdo, $twig),
    "cart"          => new Controllers\CartController($pdo, $twig),
];
$routes[$view]->index();

echo $twig->render('footer.twig');

